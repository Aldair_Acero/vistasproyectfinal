const app = angular.module("myApp", ["ngRoute"]);

app.config(function ($routeProvider) {
    var basePath = '/app/views/'

    $routeProvider
        .when("/", {
            templateUrl: './app/views/main.html',
            controller: "homeController"
        })
        .when("/getHours", {
            templateUrl: './app/views/getHours.html',
            controller: "getHoursController"
        })
        .when("/getByISAndDates", {
            templateUrl: './app/views/getByISAndDates.html',
            controller: "getByISAndDatesController"
        })
        .when("/getByMonth", {
            templateUrl: './app/views/getByMonth.html',
            controller: "getByMonthController"
        })
        .when("/getByDates", {
            templateUrl: './app/views/getByDates.html',
            controller: "getByDatesController"
        })
        .otherwise({
            templateUrl: './app/views/error.html'
        });
});

app.controller('homeController', function ($scope,$location) {
    $scope.hello = "Vistas proyecto final"
 });

 app.controller('getHoursController', function ($scope,$http) {
    $scope.hello = "Get Hours module"
    $scope.IS = "CXMG"
    $scope.month = "01"

    $scope.consultar = function(){
        console.log($scope.IS + " " + $scope.month)
        var IS = $scope.IS
        var m = $scope.month
        $http({

            method: 'GET',
            
            url: 'http://localhost:8080/proyectFinal-0.0.1-SNAPSHOT/api/v1/users/'+IS+'/'+m
            
            }).then(function success(response) {
                console.log(response)
                $scope.horas = response.data[0]
            
            }, function error(response) {
                console.log(response)
                alert(response.data.message + " Status: " + response.status)
                $scope.horas = ""
            });
    }

 });

 app.controller('getByISAndDatesController', function ($scope,$http) {
    $scope.hello = "Get By IS And Dates module"
    
    $scope.data = {
        "is_emp": "GAAN",
        "fecha_entrada": "2019-01-19",
        "fecha_salida": "2019-02-19"
    }

    $scope.consultar = function(){
        console.log($scope.data.fecha_entrada + " " + $scope.data.fecha_salida)
        
        $http({

            method: 'POST',
            
            url: 'http://localhost:8080/proyectFinal-0.0.1-SNAPSHOT/api/v1/users',
            data: $scope.data
            
            }).then(function success(response) {
                console.log(response)
                $scope.datos = response.data
            
            }, function error(response) {
                console.log(response)
                alert(response.data.message + " Status: " + response.status)
                $scope.datos = ""
            });
    }
 });

 app.controller('getByMonthController', function ($scope,$http) {
    $scope.hello = "Get By Month module"
    
    $scope.month = "01"

    $scope.consultar = function(){
        console.log($scope.month)
        var m = $scope.month
        $http({

            method: 'GET',
            
            url: 'http://localhost:8080/proyectFinal-0.0.1-SNAPSHOT/api/v1/period/'+m
            
            }).then(function success(response) {
                console.log(response)
                $scope.datos = response.data
            
            }, function error(response) {
                console.log(response)
                alert(response.data.message + " Status: " + response.status)
                $scope.datos = ""
            });
    }
 });

 app.controller('getByDatesController', function ($scope,$http) {
    $scope.hello = "Get By Dates module"

    $scope.date = {
        "fecha_entrada": "2019-01-19",
        "fecha_salida": "2019-02-19"
    }

    $scope.consultar = function(){
        console.log($scope.date.fecha_entrada + " " + $scope.date.fecha_salida)
        
        $http({

            method: 'POST',
            
            url: 'http://localhost:8080/proyectFinal-0.0.1-SNAPSHOT/api/v1/period',
            data: $scope.date
            
            }).then(function success(response) {
                console.log(response)
                $scope.datos = response.data
            
            }, function error(response) {
                console.log(response)
                alert(response.data.message + " Status: " + response.status)
                $scope.datos = ""
            });
    }
 });