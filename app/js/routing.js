import { app } from './module.js';

app.config(function ($routeProvider) {
    var basePath = '/app/views/'

    $routeProvider
        .when("/", {
            templateUrl: '../views/main.html',
            controller: "homeController"
        })
        .when("/getHours", {
            templateUrl: '../views/getHours.html',
            controller: "homeController"
        })
        .otherwise({
            templateUrl: '../views/main.html'
        });
});